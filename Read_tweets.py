import findspark
findspark.init()
from pyspark.sql import SparkSession
from pyspark.sql.functions import expr


spark = SparkSession.builder.appName("TwitterHashtagCount").getOrCreate()

# initiate streaming text from a TCP (socket) source:
socket_stream = spark.readStream.format("socket").option("host", "127.0.0.1").option("port", 5555).load()

hashtag_counts = socket_stream \
                .selectExpr("explode(split(value, ' ')) as word") \
                .where(expr("lower(word) like '#%'")) \
                .groupBy("word") \
                .count() \
                .withColumnRenamed("word", "hashtag") \
                .orderBy(expr("count desc")) \
                .limit(10)

# Start the streaming query and write the results to a temporary table
query = hashtag_counts.writeStream \
        .outputMode("complete") \
        .format("memory") \
        .queryName("hashtagTable") \
        .start()

# Wait for the query to terminate
query.awaitTermination()

# Stop the Spark session
spark.stop()