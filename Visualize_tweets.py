import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

# Retrieve the data from the temporary table
spark = SparkSession.builder.appName("VisualizeHashtags").getOrCreate()
hashtags_df = spark.sql("SELECT * FROM hashtagsTable")

# Convert Spark DataFrame to Pandas DataFrame
hashtags_pd = hashtags_df.toPandas()

# Plot using Seaborn and Matplotlib
plt.figure(figsize=(10, 6))
sns.barplot(x="hashtag", y="count", data=hashtags_pd)
plt.xticks(rotation=45)
plt.xlabel("Hashtag")
plt.ylabel("Count")
plt.title("Top 10 Hashtags")
plt.tight_layout()
plt.show()

# Stop the Spark session
spark.stop()